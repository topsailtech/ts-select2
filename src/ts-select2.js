// WARNING: This custom element uses JQuery!
//          It also relies on the select2 jquery plugin to be loaded (happems in ts-select2.html).

import 'select2'
import {default as jQuery} from "jquery"
import WC from 'wc-util'
import init_drag_and_drop from './drag_and_drop'
import init_option_count from './option_count'

class TsSelect2 extends HTMLSelectElement {
  connectedCallback(){
    //
    //  Translate JQuery's custom "change" event to a regular CustomEvent (otherwise we can not attach a
    //    plain event listener). Be aware that the onchange attribute now will be invoked for the light-weight
    //    JQuery change event AND the actual chnage CustomEvent!
    //
    jQuery(this).on("change", function(e){
      if (e.isTrigger){
        WC.fireEvent(this, "change");
        return false;
      }
    });

    //
    //  Initialize the JQuery Select2 control
    //
    if (document.readyState == 'loading'){ // we don't have enough information about the select's style yet => wait
      document.addEventListener("DOMContentLoaded", ()=>this._initialize())
    } else {
      this._initialize();
    };
  }

  reinitialize(){
    jQuery(this).select2("destroy");
    let sel2_opts = this._initialize_options();
    jQuery(this).select2(sel2_opts);
    /* I don't think we need to reinitialize DND, since those events stay in place */
  }

  _initialize(){
    let sel2_opts = this._initialize_options();
    jQuery(this).select2(sel2_opts);
    if (this.hasAttribute("data-orderable")){ init_drag_and_drop.bind(this)() }
    if (this.hasAttribute("data-count-start")){ init_option_count.bind(this)() }
  }

  _initialize_options(){
    let sel2_opts = {};

    // Set allow-clear and placeholder.
    // For this to work, the <option> children must be loaded (at least the first).
    // - on initial page load this is true since we wait for jQuery(document).ready()
    // - on XHR it _seems_ to work too, but I am not sure how that can be
    let first_option = this.options[0];
    if (!this.hasAttribute("data-allow-clear") && first_option && first_option.value ==''){
      this.setAttribute("data-allow-clear", true);
    }

    if (!this.hasAttribute("data-placeholder") && first_option && first_option.value ==''){
      this.setAttribute("data-placeholder", first_option.innerHTML);
    }

    //
    // Set Ajax default options
    //
    if (this.hasAttribute("data-ajax--url")){
      sel2_opts['ajax'] = {
        data: this._data_fn.bind(this),
        processResults: (
                          WC.function_named(this.getAttribute("data-ajax--processResults")) ||
                          this._processResults_fn
                        ).bind(this)
      };
      sel2_opts['escapeMarkup']      = function (markup) { return markup; }; // let our custom formatter work
      sel2_opts['templateSelection'] = this._ajax_templateSelection_fn;
      sel2_opts['templateResult']    = this._ajax_templateResult_fn;

      if (!this.hasAttribute("data-ajax--delay")) this.setAttribute("data-ajax--delay", 250);
      if (!this.hasAttribute("data-minimum-input-length")) this.setAttribute("data-minimum-input-length", 2);

      this.per_page = this.getAttribute("data-per-page") || 20;
      this.search_key = this.getAttribute("data-search-key") || 'q';
    } else {
      //
      // non-ajax config
      //
      // Using suffix "-fn" since oterwise the existing data attribute overshaddows the selet2_option
      let tmpl_result_fn = WC.function_named(this.getAttribute("data-template-result-fn"));
      if (tmpl_result_fn){
        sel2_opts["templateResult"] = tmpl_result_fn;
      }
      // Using suffix "-fn" since oterwise the existing data attribute overshaddows the selet2_option
      let tmpl_selection_fn = WC.function_named(this.getAttribute("data-template-selection-fn"));
      if (tmpl_selection_fn){
        sel2_opts["templateSelection"] = tmpl_selection_fn;
      }
    }

    //
    //  Expose a few select2 options that are not (yet?) fully exposed via data attributes
    //
    if (this.hasAttribute("data-dropdown-parent-selector")){
      let dropdown_sel = this.getAttribute("data-dropdown-parent-selector"),
          $dropdown_paret_el = jQuery(dropdown_sel);
      if ($dropdown_paret_el.length == 0){
        console.warn("Dropdown Parent " + dropdown_sel + " missing", this);
      } else {
        sel2_opts['dropdownParent'] = $dropdown_paret_el;
      }
    }
    // HACK for <dialog> element, since Top Layer makes the dropdown inaccessible
    if (!sel2_opts['dropdownParent']){
      var ancestor_dialog = this.closest("dialog");
      if (ancestor_dialog){ sel2_opts['dropdownParent'] = jQuery(ancestor_dialog) }
    }
    return sel2_opts;
  }

  _data_fn(params){ // page is the one-based page number tracked by Select2
    var req_params = {
      filter: {},
      paginate: {page: params.page, per_page: this.per_page}
    };
    req_params['filter'][this.search_key] = params.term;
    return req_params;
  }

  _processResults_fn(data, params){
    params.page = params.page || 1;
    return { results: data.records, pagination: {more: (params.page * this.per_page) < data.total}  };
  }

  // what to show once a row in the drop down was selected (record is what came back from Ajax)
  // templateSelection must work for AJAX results AND for possible initial selected OPTION
  _ajax_templateSelection_fn(record){
    return record.short_html || record.html || record.text;
  }

  // rows in the select2 dropdown from ajax result records
  _ajax_templateResult_fn(record){
    return record.long_html || record.html || record.text;
  }
};

customElements.define('ts-select2', TsSelect2, { extends: "select" });