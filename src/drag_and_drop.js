import {default as jQuery} from "jquery"

export default init_drag_and_drop

/*
  if the SELECT has attribute "data-orderable" and is multiple=true,
  we can rearrange options via DND.
*/
function init_drag_and_drop(){
  this.sortable_list = this.nextElementSibling.querySelector('ul.select2-selection__rendered');

  // make the current list itmes draggable
  // we have to make sure this gets re-done each time the list is re-generated
  jQuery(this).on("select2:open select2:close change.select2", () => make_li_draggable(this.sortable_list));

  // preserve order of insertion
  jQuery(this).on('select2:select', function(e){
    move_to_end_of_sortable_list.bind(this)(e.params.data.element);
    jQuery(this).trigger('change.select2');
  });

   // prevent default for dragover to allow drop
   this.sortable_list.addEventListener("dragover", (e)=>e.preventDefault());

  this.sortable_list.addEventListener("dragstart", handle_dragstart.bind(this));
  this.sortable_list.addEventListener("dragenter", handle_dragenter.bind(this));
  this.sortable_list.addEventListener("dragleave", handle_dragleave.bind(this));
  this.sortable_list.addEventListener("drop",      handle_drop.bind(this));

  this.sortable_list.addEventListener("dragend",   handle_dragend.bind(this));
  make_li_draggable(this.sortable_list);
}

function handle_dragenter(evt){
  // mark list items before which we can drop the element
  let li = evt.target.closest("li");
  if (is_droppable_li(li)){
    li.classList.add("ts-select2_dragged_over");
  }
}

function handle_dragleave(evt){
  evt.target.classList.remove("ts-select2_dragged_over");
}

function handle_dragstart(evt){
  // Remember the element that will be moved (evt.dataTransfer is only for Strings or Files);
  // This also lets us style the dragged element
  evt.target.classList.add("ts-select2_dragged");
  set_up_hidden_li(this.sortable_list);
}

function handle_drop(evt){
  var dragged_li = this.sortable_list.querySelector("li.ts-select2_dragged"),
      dropped_el = evt.target;

  if (dropped_el.tagName == "LI" && dropped_el.matches(".ts-select2_drop_zone_after")) {
    dragged_li.remove();
    this.sortable_list.appendChild(dragged_li);
  } else if (!is_droppable_li(dropped_el)) {
    return;
  } else {
    // move the dragged li in the new place
    dragged_li.remove();
    this.sortable_list.insertBefore(dragged_li, dropped_el);
  }

  // adjust the actual select options accordingly
  selection_choices(this.sortable_list).forEach(li => {
    var opt = [].find.call(this.options, o => o.innerHTML == li.title);
    move_to_end_of_sortable_list(opt)
  })

  jQuery(this).trigger('change.select2');
}

function handle_dragend(evt){
  this.sortable_list.childNodes.forEach((li) => {
    li.classList.remove("ts-select2_dragged","ts-select2_dragged_over")
  })
  tear_down_hidden_li(this.sortable_list);
}

function is_droppable_li(el){
  return el &&
           el.matches("li.select2-selection__choice:not(.ts-select2_dragged)") &&
           (!el.previousElementSibling || !el.previousElementSibling.matches(".ts-select2_dragged"))
}

function make_li_draggable(sortable_list){
  // waiting a little bit to give Select2 a chance to finish rendering this DOM fragment first
  setTimeout(()=> {
    selection_choices(sortable_list).forEach(li => li.draggable = true); // make LIs draggable
  }, 10);
}

function selection_choices(sortable_list){
  return [].filter.call(sortable_list.childNodes,
                        el=>el.matches(".select2-selection__choice"));
}

// append invisible LI to handle drop _after_ last element
function set_up_hidden_li(sortable_list){
  let select2_drop_zone_after = document.createElement("li")
  select2_drop_zone_after.classList.add("ts-select2_drop_zone_after")
  sortable_list.append(select2_drop_zone_after)
}

function tear_down_hidden_li(sortable_list){
  sortable_list.querySelectorAll("li.ts-select2_drop_zone_after").
    forEach(li => li.remove())
}

function move_to_end_of_sortable_list(select_option){
  let select = select_option.closest("select");
  select_option.remove();
  select.append(select_option);
}
