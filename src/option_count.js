import {default as jQuery} from "jquery"

export default init_option_count

//
//  Support for data-count-start and data-count-label
//
function init_option_count(){
  var $selection_rendered = jQuery(this.nextElementSibling.querySelector('ul.select2-selection__rendered')),
      cnt = jQuery(this).find("option:selected").length;
  if (cnt >= this.getAttribute("data-count-start")){
    $selection_rendered.find("li.select2-selection__choice").remove();
    $selection_rendered.prepend("<li class='select2-selection__choice select2__count'>(" + cnt +
       " " + (this.getAttribute("data-count-label") || "items") + ")</li>");
  }
}
