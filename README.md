# Installation

```
yarn add https://bitbucket.org/topsailtech/ts-select2
```
(you might need to add the private topsail_ro_accesskey to your workstation)

# Usage

In your pack, add
```
require('ts-select2')
```
and reference this pack as a javascript *and* a css resource.

You now can add attribute is="ts-select2" to any html SELECT DOM element.